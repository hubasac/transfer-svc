FROM azul/zulu-openjdk-alpine:11.0.6

LABEL MAINTAINER KiyanjaLabs.org

EXPOSE 8081

RUN mkdir -p /srv/asac/app/hubasac/transfer-svc/lib /srv/asac/app/hubasac/transfer-svc/scripts /srv/asac/app/hubasac/transfer-svc/config \
    mkdir -p /var/log/hubasac

ADD ./scripts* /srv/asac/app/hubasac/transfer-svc/scripts
ADD ./config* /srv/asac/app/hubasac/transfer-svc/config
ADD ./lib* /srv/asac/app/hubasac/transfer-svc/lib

#COPY ./lib/postgresql-42.2.26.jre7.jar ./lib/postgresql-42.2.26.jre7.jar
COPY ./scripts/transferMinfiData.sh /srv/asac/app/hubasac/extract-svc/scripts/transferMinfiData.sh
COPY ./target/application.properties /srv/asac/app/hubasac/transfer-svc/application.properties
COPY ./target/minfi-0.0.1-SNAPSHOT.jar /srv/asac/app/hubasac/transfer-svc/minfi-0.0.1-SNAPSHOT.jar

WORKDIR /srv/asac/app/hubasac/transfer-svc

#ENTRYPOINT ["java", "-jar", "-Dspring.config.location=./application.properties", "./minfi-0.0.1-SNAPSHOT.jar"]
CMD ["chmod", "+x", "/srv/asac/app/hubasac/transfer-svc/scripts/runTransformer.sh"]

ENTRYPOINT ["/bin/sh","/srv/asac/app/hubasac/transfer-svc/scripts/runTransfer.sh"]